<?php
namespace Repositories\Contracts;

interface MessageRepositoryInterface
{
    public function getAll();
    public function getTotalUnread();
    public function getTotalRead();
    public function setReaded(Message $message);
    public function setUnreaded(Message $message);
    public function isReaded(Message $message);
}