<?php

namespace Repositories;

use Models\Message;
use Contracts\MessageRepositoryInterface;

class MessageRepository implements MessageRepositoryInterface
{
    public function getAll() {
        return Message::all();
    }

    public function getTotalUnread() {
        return Message::where('is_readed', 0)->count();
    }

    public function getTotalRead() {
        return Message::where('is_readed', 1)->count();
    }

    public function setReaded(Message $message) {
        if ( !$this->isReaded($message) ) {
            $message->save(['is_readed' => 1]);
        }
    }

    public function setUnreaded(Message $message) {
        if ( $this->isReaded($message) ) {
            $message->save(['is_readed' => 0]);
        }
    }

    public function isReaded(Message $message) {
        return ($message->is_readed == 1);
    }
}