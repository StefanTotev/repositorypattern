<?php
namespace Controllers;

use App\Http\Controllers\Controller;
use Repositories\MessageRepository;
use Models\Message;
use function compact;

class MessagesController extends Controller
{
    // $message идва от рутера на Ларавел като ми връща резултат от (например) Message::where('id', 1)->get()
    // И тука в този случай използвам MessageRepository да отчета съобщението като прочетено
    public function show(Message $message, MessageRepository $messageRepository) {
        $messageRepository->setReaded($message);
        return view('messages.show', compact('message'));
    }
}